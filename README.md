3ch3r46/bootstrap-datepicker
============================
The Bootstrap Datepicker extension for yii framework

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist 3ch3r46/bootstrap-datepicker "*"
```

or add

```
"3ch3r46/bootstrap-datepicker": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \a3ch3r46\datepicker\Datepicker::widget(['selector' => ['form-input_example']]); ?>
```

