<?php

namespace a3ch3r46\datepicker;

use yii\helpers\Html;
use yii\helpers\Json;
/**
 * Datepicker class name.
 * @author moh khoirul anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class Datepicker extends \yii\base\Widget
{
	/**
	 * @var string input selector
	 */
	public $selector;
	/**
	 * @var yii\base\Model
	 */
	public $model;
	/**
	 * @var string attribute model
	 */
	public $attribute;
	/**
	 * @var string name
	 */
	public $name;
	/**
	 * @var string template form
	 */
	public $template = '<div class="form-group">{label} {input}</div>';
	/**
	 * @var string set an id widget
	 */
	public $id;
	/**
	 * @var array set a label options and enabling a label.
	 */
	public $_labelOptions = [];
	/**
	 * @var string set a date format.
	 */
	public $format = 'YYYY-MM-DD';
	/**
	 * @var boolean to use an icon group
	 */
	public $enableIcon = false;
	/**
	 * @var boolean enables/disables the time picker
	 */
	public $pickTime = false;
	/**
	 * @var boolean enables/disables the date picker
	 */
	public $pickDate = true;
	/**
	 * @var boolean enables/disables the minute picker
	 */
	public $useMinutes;
	/**
	 * @var boolean enables/disables the second picker
	 */
	public $useSeconds;
	/**
	 * @var boolean when true, picker will set the value to the current date/time
	 */
	public $useCurrent;
	/**
	 * @var integer set the minute stepping
	 */
	public $minuteStepping;
	/**
	 * @var string set a minimum date
	 */
	public $minDate;
	/**
	 * @var string set a maximum date (defaults to today +100 years)
	 */
	public $maxDate;
	/**
	 * @var boolean shows the today indicator
	 */
	public $showToday;
	/**
	 * @var string sets a default date, accepts js dates, strings and moment objects
	 */
	public $defaultDate;
	/**
	 * @var array of dates that cannot be selected
	 */
	public $disabledDates;
	/**
	 * @var array of dates that can be selected
	 */
	public $enabledDates;
	/**
	 * @var 
	 */
	public $icons;
	/**
	 * @var boolean use "strict" when validating dates  
	 */
	public $useStrict;
	/**
	 * @var boolean show the date and time picker side by side
	 */
	public $sideBySide;
	/**
	 * @var array for example use daysOfWeekDisabled: [0,6] to disable weekends
	 */
	public $daysOfWeekDisabled;
	/**
	 * @var string sets language locale. default 'en'.
	 */
	public $language;
	
	protected $pluginOptions = [];

	/**
	 * @var array the event handlers for the underlying Bootstrap Javascript plugin.
	 * Please refer to the corresponding Bootstrap plugin Web page for possible events.
	 * Events :
	 * ~~~
	 * `dp.change` Fires when the datepicker `changes` or updates the date. Note that change may also fire for knockout support.
	 * `dp.show` Fires when the widget is shown.
	 * `dp.hide` Fires when the widget is hidden.
	 * `dp.error` Fires when Moment cannot parse the date or when the timepicker cannot change because of a `disabledDates` setting. Returns a Moment date object. The specific error can be found be using invalidAt(). For more information see Moment's docs
	 * ~~~
	 */
	public $jsEvents = [];

	/**
	 * @var array the options for the underlying Bootstrap Javascript plugin.
	 * Please refer to the corresponding Bootstrap plugin Web page for possible options.
	 */
	public $jsOptions = [];
	
	public function init()
	{
		parent::init();
		$this->prepareOptions();

		Html::addCssClass($this->_labelOptions, 'control-label');
		
		if (isset($this->model)) {
			$this->id = Html::getInputId($this->model, $this->attribute);
		}
	}
	
	protected function prepareOptions()
	{
		if (isset($this->pickDate))
			$this->pluginOptions['pickDate'] = $this->pickDate;
		
		if (isset($this->pickTime))
			$this->pluginOptions['pickTime'] = $this->pickTime;
		
		if (isset($this->useMinutes))
			$this->pluginOptions['useMinutes'] = $this->useMinutes;
		
		if (isset($this->useSeconds))
			$this->pluginOptions['useSeconds'] = $this->useSeconds;

		if (isset($this->useCurrent))
			$this->pluginOptions['useCurrent'] = $this->useCurrent;
		
		if (isset($this->minuteStepping))
			$this->pluginOptions['minuteStepping'] = $this->minuteStepping;

		if (isset($this->minDate))
			$this->pluginOptions['minDate'] = $this->minDate;

		if (isset($this->maxDate))
			$this->pluginOptions['maxDate'] = $this->maxDate;
		
		if (isset($this->showToday))
			$this->pluginOptions['showToday'] = $this->showToday;

		if (isset($this->language))
			$this->pluginOptions['language'] = $this->language;

		if (isset($this->defaultDate))
			$this->pluginOptions['defaultDate'] = $this->defaultDate;

		if (isset($this->disabledDates))
			$this->pluginOptions['disabledDates'] = $this->disabledDates;
		
		if (isset($this->enabledDates))
			$this->pluginOptions['enabledDates'] = $this->enabledDates;

		if (isset($this->icons))
			$this->pluginOptions['icons'] = $this->icons;

		if (isset($this->useStrict))
			$this->pluginOptions['useStrict'] = $this->useStrict;

		if (isset($this->sideBySide))
			$this->pluginOptions['sideBySide'] = $this->sideBySide;

		if (isset($this->daysOfWeekDisabled))
			$this->pluginOptions['daysOfWeekDisabled'] = $this->daysOfWeekDisabled;
		
		if (isset($this->format))
			$this->pluginOptions['format'] = $this->format;
	}
	
	public function setLabelOptions($value)
	{
		$this->_labelOptions = array_merge($this->_labelOptions, $value);
	}
	
	public function setWrapper($value)
	{
		if (isset($value['label']))
			Html::addCssClass($this->_labelOptions, $value['label']);
		if (isset($value['input']))
			$this->template = strtr($this->template, ['{input}' => '<div class="' . $value['input'] . '">{input}</div>']);
	}
	
	public $enableLabel = true;
	
	public function prepareWidget()
	{
		$label = '';
		if ($this->enableLabel && isset($this->model)) {
			$label = Html::activeLabel($this->model, $this->attribute, $this->_labelOptions);
		}
		if ($this->enableIcon) {
			$this->template = strtr($this->template, ['{input}' => '<div class="input-group date" data-date-format="' . $this->format . '">{input}<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>']);
			$input = Html::activeTextInput($this->model, $this->attribute, ['class' => 'form-control']);
		} else {
			$input = Html::activeTextInput($this->model, $this->attribute, ['class' => 'form-control']);
		}
		
		return strtr($this->template, ['{input}' => $input, '{label}' => $label]);
	}
	
    public function run()
    {
    	$this->prepareSelector();
    	
    	if (!isset($this->selector)) {
    		$this->registerPlugin();
    		return $this->prepareWidget();
    	}
    }
    
    public function prepareSelector()
    {
    	if (is_array($this->selector))
    	{
    		foreach ($this->selector as $id)
    		{
    			$this->id = $id;
    			$this->registerPlugin();
    		}
    	} else {
    		$this->id = $id;
    		$this->registerPlugin();
    	}
    }
    
    public function registerPlugin()
    {
    	$view = $this->getView();

    	DatepickerPlugin::register($view);
    	
    	if (!isset($this->id))
    		$this->id = $this->options['id'];
    	
    	$id = $this->id;
    	
    	$this->jsOptions = array_merge($this->jsOptions, $this->pluginOptions);
    	
    	if ($this->jsOptions !== false) {
    		$options = empty($this->jsOptions) ? '' : Json::encode($this->jsOptions);
    		$js = "jQuery('#$id').datetimepicker($options);";
    		$view->registerJs($js);
    	}
    	
    	if (!empty($this->jsEvents)) {
    		$js = [];
    		foreach ($this->jsEvents as $event => $handler) {
    			$js[] = "jQuery('#$id').on('$event', $handler);";
    		}
    		$view->registerJs(implode("\n", $js));
    	}
    }
}
