<?php
namespace a3ch3r46\datepicker;

use yii\web\AssetBundle;

class DatepickerPlugin extends AssetBundle
{
	public $sourcePath = '@a3ch3r46/datepicker/dist';
	
	public $css = [
		'css/bootstrap-datetimepicker.min.css',
	];

	public $js = [
		'js/moment.js',
		'js/bootstrap-datetimepicker.min.js',
	];
	
	public $depends = [
		'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];
}
